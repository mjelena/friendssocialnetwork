package com.example.jelena.friendssocialnetwork;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private TextView username, userFullName, userStatus, userGender, userCountry, userRelationShip, userDoB;
    private CircleImageView userProfImage;
    private DatabaseReference profileUserRef, friendsRef, postRef;
    private FirebaseAuth mAuth;
    private String currentUserID;
    private Button myPosts, myFriends;
    private int countFriends = 0, countPost = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        profileUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserID);
        friendsRef = FirebaseDatabase.getInstance().getReference().child("Friends");
        postRef = FirebaseDatabase.getInstance().getReference().child("Posts");

        username = findViewById(R.id.my_profile_username);
        userFullName = findViewById(R.id.my_profile_fullname);
        userStatus = findViewById(R.id.my_profile_status);
        userCountry = findViewById(R.id.my_country);
        userDoB = findViewById(R.id.my_dateOfBirth);
        userRelationShip = findViewById(R.id.my_relationship_status);

        userProfImage = findViewById(R.id.my_profile_picture);
        userGender = findViewById(R.id.my_gender);
        myFriends = findViewById(R.id.my_friends_button);
        myPosts = findViewById(R.id.my_post_button);

        myFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               sendUserToFriendsActivity();
            }
        });

        myPosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUserToMyPostActivity();
            }
        });

        friendsRef.child(currentUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    countFriends = (int) dataSnapshot.getChildrenCount();
                    myFriends.setText(Integer.toString(countFriends ) + " friends");
                } else {
                    myFriends.setText("0 friends");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        postRef.child(currentUserID).orderByChild("uid")
                .startAt(currentUserID).endAt(currentUserID + "\uf8ff")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            countPost = (int) dataSnapshot.getChildrenCount();
                            myPosts.setText(Integer.toString(countPost) + " posts");

                        } else {
                            myPosts.setText("0 posts");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        profileUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String myProfileImage = dataSnapshot.child("profileImage").getValue().toString();
                    String myUserName = dataSnapshot.child("username").getValue().toString();
                    String myProfileName = dataSnapshot.child("fullname").getValue().toString();
                    String myProfileStatus = dataSnapshot.child("status").getValue().toString();
                    String myCountry = dataSnapshot.child("country").getValue().toString();
                    String myDoB = dataSnapshot.child("dob").getValue().toString();
                    String myRelationStatus = dataSnapshot.child("relationshipStatus").getValue().toString();
                    String myGender = dataSnapshot.child("gender").getValue().toString();

                    Picasso.get().load(myProfileImage).placeholder(R.drawable.profile_picture).into(userProfImage);
                    username.setText("@" +myUserName);
                    userFullName.setText(myProfileName);
                    userStatus.setText(myProfileStatus);
                    userDoB.setText("Date of birth: " + myDoB);
                    userCountry.setText("Country: " + myCountry);
                    userGender.setText("Gender: " + myGender);
                    userRelationShip.setText("Relationship status: "+ myRelationStatus);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendUserToMyPostActivity() {
        Intent myPostIntent = new Intent(ProfileActivity.this,  MyPostActivity.class);
        startActivity(myPostIntent);
    }

    private void sendUserToFriendsActivity() {
        Intent findFriendsIntent = new Intent(ProfileActivity.this,  FriendsActivity.class);
        startActivity(findFriendsIntent);
    }
}
