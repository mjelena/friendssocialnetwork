package com.example.jelena.friendssocialnetwork;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindFriendsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageButton searchButton;
    private EditText searchInputText;
    private RecyclerView searchResultList;
    private DatabaseReference usersRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        mToolbar =  findViewById(R.id.find_friends_bar_layout);
        setSupportActionBar(mToolbar);

        usersRef  = FirebaseDatabase.getInstance().getReference().child("Users");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);;
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Find friends");

        searchResultList = findViewById(R.id.search_result_list);
        searchResultList.setLayoutManager(new LinearLayoutManager(this));

        searchButton = findViewById(R.id.search_people_friends_button);
        searchInputText = findViewById(R.id.search_box_input);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchBox = searchInputText.getText().toString();
                searchFriends(searchBox);
            }
        });
        searchFriends("");
    }

    private void searchFriends(String searchBox) {

        Toast.makeText(this, "Searching...", Toast.LENGTH_SHORT).show();

        Query searchFriendsQuery = usersRef.orderByChild("fullname").startAt(searchBox).endAt(searchBox + "\uf8ff");

        FirebaseRecyclerOptions<FindFriends> options =
                new FirebaseRecyclerOptions.Builder<FindFriends>()
                        .setQuery(searchFriendsQuery, FindFriends.class)
                        .build();
        FirebaseRecyclerAdapter<FindFriends, FindFriendsViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<FindFriends, FindFriendsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FindFriendsViewHolder holder, final int position, @NonNull FindFriends model) {

                holder.setFullname(model.getFullname());
                holder.setStatus(model.getStatus());
                holder.setProfileImage(model.getProfileImage());

                holder.mVIew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String visitUserID = getRef(position).getKey();
                        Intent profileIntent = new Intent(FindFriendsActivity.this, PersonProfileActivity.class);
                        profileIntent.putExtra("name", "A2");
                        profileIntent.putExtra("visitUserID", visitUserID);
                        startActivity(profileIntent);

                    }
                });

            }

            @NonNull
            @Override
            public FindFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_users_display_layout, parent, false);
                FindFriendsActivity.FindFriendsViewHolder findFriendsViewHolder = new FindFriendsActivity.FindFriendsViewHolder(view);
                return findFriendsViewHolder;
            }
        };
        searchResultList.setAdapter(firebaseRecyclerAdapter);
        firebaseRecyclerAdapter.startListening();

    }

    public static class FindFriendsViewHolder extends RecyclerView.ViewHolder {


        View mVIew;
        public FindFriendsViewHolder(View itemView) {
            super(itemView);
            this.mVIew = itemView;
        }

        public void setProfileImage(String profileImage) {
            CircleImageView image =  mVIew.findViewById(R.id.all_users_profile_image);
            Picasso.get().load(profileImage).into(image);
        }

        public void setFullname(String fullname) {
            TextView username =  mVIew.findViewById(R.id.all_users_profile_name);
            username.setText(fullname);

        }

        public void setStatus(String status) {
            TextView statusUser =  mVIew.findViewById(R.id.all_users_profile_status);
            statusUser.setText(status);
        }
    }

}
