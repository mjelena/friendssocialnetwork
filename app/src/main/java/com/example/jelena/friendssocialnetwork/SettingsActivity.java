package com.example.jelena.friendssocialnetwork;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private EditText username, userFullName, userStatus, userGender, userCountry, userRelationShip, userDoB;
    private Button updateAccountSettingsButton;
    private CircleImageView userProfImage;
    private ProgressDialog loadingBar;


    private DatabaseReference settingsUserRef;
    private FirebaseAuth mAuth;
    private String currentUserID;
    private StorageReference userProfileImageRef;

    final static int GALLERY_PICK = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        settingsUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserID);

        loadingBar = new ProgressDialog(this);
        userProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Images");

        mToolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Account settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        username = findViewById(R.id.settings_username);
        userFullName = findViewById(R.id.settings_profile_fullname);
        userStatus = findViewById(R.id.settings_status);
        userCountry = findViewById(R.id.settings_country);
        userDoB = findViewById(R.id.settings_dob);
        userRelationShip = findViewById(R.id.settings_relationship_status);
        updateAccountSettingsButton = findViewById(R.id.update_account_settings_button);
        userProfImage = findViewById(R.id.settings_profile_image);
        userGender = findViewById(R.id.settings_gender);


        settingsUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String myProfileImage = dataSnapshot.child("profileImage").getValue().toString();
                    String myUserName = dataSnapshot.child("username").getValue().toString();
                    String myProfileName = dataSnapshot.child("fullname").getValue().toString();
                    String myProfileStatus = dataSnapshot.child("status").getValue().toString();
                    String myCountry = dataSnapshot.child("country").getValue().toString();
                    String myDoB = dataSnapshot.child("dob").getValue().toString();
                    String myRelationStatus = dataSnapshot.child("relationshipStatus").getValue().toString();
                    String myGender = dataSnapshot.child("gender").getValue().toString();

                    Picasso.get().load(myProfileImage).placeholder(R.drawable.profile_picture).into(userProfImage);
                    username.setText(myUserName);
                    userFullName.setText(myProfileName);
                    userStatus.setText(myProfileStatus);
                    userDoB.setText(myDoB);
                    userCountry.setText(myCountry);
                    userGender.setText(myGender);
                    userRelationShip.setText(myRelationStatus);






                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        updateAccountSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAccountInfo();
            }
        });

        userProfImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_PICK);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_PICK && resultCode == RESULT_OK && data != null) {

            Uri imageUrl = data.getData();

            CropImage.activity(imageUrl)
                    .start(this);

        }
        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);


            if (resultCode == RESULT_OK) {
                loadingBar.setTitle("Profile image");
                loadingBar.setMessage("Please wait, while we are updating your profile picture...");
                loadingBar.setCanceledOnTouchOutside(true);
                loadingBar.show();


                Uri resultUri = result.getUri();

                StorageReference filePath = userProfileImageRef.child(currentUserID + ".jpg");
                filePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(SettingsActivity.this, "Profile image stored successfully in firebase storage", Toast.LENGTH_SHORT).show();
                            final String downloadUrl = task.getResult().getDownloadUrl().toString();


                            settingsUserRef.child("profileImage").setValue(downloadUrl)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Intent selfIntent = new Intent(SettingsActivity.this, SettingsActivity.class);
                                                startActivity(selfIntent);

                                                Toast.makeText(SettingsActivity.this, "Profile image stored to firebase database successfully. ", Toast.LENGTH_SHORT).show();
                                                loadingBar.dismiss();
                                            } else {
                                                String message = task.getException().getMessage();
                                                Toast.makeText(SettingsActivity.this, "Error occured: " + message, Toast.LENGTH_SHORT).show();
                                                loadingBar.dismiss();
                                            }
                                        }
                                    });
                        }
                    }
                });
            } else {
                Toast.makeText(this, "Error occured: Image can not be cropped. Try again. ", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void validateAccountInfo() {

        String usernameVal = username.getText().toString();
        String fullnameVal = userFullName.getText().toString();
        String statusVal = userStatus.getText().toString();
        String countryVal = userCountry.getText().toString();
        String dobVal = userDoB.getText().toString();
        String genderVal = userGender.getText().toString();
        String relationshipVal = userRelationShip.getText().toString();

        if(TextUtils.isEmpty(usernameVal)){
            Toast.makeText(this, "Please write your username", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(fullnameVal)){
            Toast.makeText(this, "Please write your fullname", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(statusVal)){
            Toast.makeText(this, "Please write your status", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(countryVal)){
            Toast.makeText(this, "Please write your country name", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(dobVal)){
            Toast.makeText(this, "Please write your date of birth", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(genderVal)){
            Toast.makeText(this, "Please write your gender", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(relationshipVal)){
            Toast.makeText(this, "Please write your relationship status", Toast.LENGTH_SHORT).show();
        } else {

            loadingBar.setTitle("Profile image");
            loadingBar.setMessage("Please wait, while we are updating your profile picture...");
            loadingBar.setCanceledOnTouchOutside(true);
            loadingBar.show();
            updateAccoutInfo(usernameVal, fullnameVal, statusVal, countryVal, dobVal, genderVal, relationshipVal);
        }
    }

    private void updateAccoutInfo(String usernameVal, String fullnameVal, String statusVal, String countryVal, String dobVal, String genderVal, String relationshipVal) {
        HashMap userMap = new HashMap();
        userMap.put("username", usernameVal);
        userMap.put("fullname", fullnameVal);
        userMap.put("country", countryVal);
        userMap.put("dob", dobVal);
        userMap.put("gender", genderVal);
        userMap.put("relationshipStatus", relationshipVal);
        userMap.put("status", statusVal);

        settingsUserRef.updateChildren(userMap).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if(task.isSuccessful()){
                    Toast.makeText(SettingsActivity.this, "Account settings updated successfully", Toast.LENGTH_SHORT ).show();
                    sendUserToMainActivity();

                } else {
                    Toast.makeText(SettingsActivity.this, "Error occured while updating account information.", Toast.LENGTH_SHORT ).show();

                }
                loadingBar.dismiss();
            }
        });


    }

    private void sendUserToMainActivity() {
        Intent mainIntent = new Intent(SettingsActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }
}
