package com.example.jelena.friendssocialnetwork;

public class Posts {

    public String data;
    public String description;
    public String fullName;
    public String time;
    public String postImage;
    public String profileImage;
    public String uid;
    public long counter;

    public Posts(){

    }

    public Posts(String data, String description, String fullName, String time, String postImage, String profileImage, String uid, long counter) {
        this.data = data;
        this.description = description;
        this.fullName = fullName;
        this.time = time;
        this.postImage = postImage;
        this.profileImage = profileImage;
        this.uid = uid;
        this.counter = counter;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }
}
