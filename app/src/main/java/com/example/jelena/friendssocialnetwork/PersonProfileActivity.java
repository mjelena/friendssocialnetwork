package com.example.jelena.friendssocialnetwork;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonProfileActivity extends AppCompatActivity {

    private TextView username, userFullName, userStatus, userGender, userCountry, userRelationShip, userDoB;
    private CircleImageView userProfImage;
    private Button sendFriendRequestBtn, declineFriendRequestBtn;
    private DatabaseReference friendRequestRef, usersRef, friendsRef;
    private FirebaseAuth mAuth;
    private String senderUserID, receiverUserID;
    private String currentState;
    private String saveCurrentData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_profile);

        mAuth = FirebaseAuth.getInstance();
        receiverUserID = getIntent().getExtras().get("visitUserID").toString();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        senderUserID = mAuth.getCurrentUser().getUid();
        friendRequestRef = FirebaseDatabase.getInstance().getReference().child("FriendRequests");
        friendsRef = FirebaseDatabase.getInstance().getReference().child("Friends");

        initializeFields();
       // onNewIntent(getIntent());

        //Intent i = getIntent();

        /*switch(i.getExtras().get("Activity").toString()) {
            case "1":
                // get the data of Activity1
                receiverUserID = i.getStringExtra("visitUserID");
                break;
            case "2":
                // get the data of Activity2
                receiverUserID = i.getStringExtra("notificationUserID");
                break;
        }



        Log.i("TAG", getIntent().getExtras().get("name").toString());
            if (getIntent().getExtras().get("name").toString().equals("A1")) {
                receiverUserID = i.getStringExtra("notificationUserID");
            } else if(getIntent().getExtras().get("name").toString().equals("A2")) {
                receiverUserID = i.getStringExtra("visitUserID");
            }


*/


        usersRef.child(receiverUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String myProfileImage = dataSnapshot.child("profileImage").getValue().toString();
                    String myUserName = dataSnapshot.child("username").getValue().toString();
                    String myProfileName = dataSnapshot.child("fullname").getValue().toString();
                    String myProfileStatus = dataSnapshot.child("status").getValue().toString();
                    String myCountry = dataSnapshot.child("country").getValue().toString();
                    String myDoB = dataSnapshot.child("dob").getValue().toString();
                    String myRelationStatus = dataSnapshot.child("relationshipStatus").getValue().toString();
                    String myGender = dataSnapshot.child("gender").getValue().toString();

                    Picasso.get().load(myProfileImage).placeholder(R.drawable.profile_picture).into(userProfImage);
                    username.setText("@" +myUserName);
                    userFullName.setText(myProfileName);
                    userStatus.setText(myProfileStatus);
                    userDoB.setText("Date of birth: " + myDoB);
                    userCountry.setText("Country: " + myCountry);
                    userGender.setText("Gender: " + myGender);
                    userRelationShip.setText("Relationship status: "+ myRelationStatus);

                    maintananceOfButton();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        declineFriendRequestBtn.setVisibility(View.INVISIBLE);
        declineFriendRequestBtn.setEnabled(false);

        if(!senderUserID.equals(receiverUserID)){
            sendFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendFriendRequestBtn.setEnabled(false);

                    if(currentState.equals("not_friends")){
                        sendFriendRequestToaPerson();
                    }
                    if(currentState.equals("request_sent")){
                        cancelFriendRequest();
                    }
                    if(currentState.equals("request_received")){
                        acceptFriendRequest();
                    }
                    if(currentState.equals("friends")){
                        unfriendExistingFriend();
                    }
                }
            });

        } else {
            sendFriendRequestBtn.setVisibility(View.INVISIBLE);
            declineFriendRequestBtn.setVisibility(View.INVISIBLE);

        }





    }

    private void unfriendExistingFriend() {

        friendsRef.child(senderUserID).child(receiverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    friendsRef.child(receiverUserID).child(senderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                sendFriendRequestBtn.setEnabled(true);
                                currentState = "not_friends";
                                sendFriendRequestBtn.setText("Send friend request");

                                declineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                declineFriendRequestBtn.setEnabled(false);
                            }
                        }
                    });
                }
            }
        });


    }

    private void acceptFriendRequest() {

        Calendar calForDate = Calendar.getInstance();
        SimpleDateFormat currentData = new SimpleDateFormat("dd-MMMM-yyyy");
        saveCurrentData = currentData.format(calForDate.getTime());

        friendsRef.child(senderUserID).child(receiverUserID).child("date").setValue(saveCurrentData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){

                    friendsRef.child(receiverUserID).child(senderUserID).child("date").setValue(saveCurrentData).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){

                                friendRequestRef.child(senderUserID).child(receiverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            friendRequestRef.child(receiverUserID).child(senderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()){
                                                        sendFriendRequestBtn.setEnabled(true);
                                                        currentState = "friends";
                                                        sendFriendRequestBtn.setText("Unfriend this person");

                                                        declineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                                        declineFriendRequestBtn.setEnabled(false);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });

                            }
                        }
                    });

                }
            }
        });

    }

    private void cancelFriendRequest() {

        friendRequestRef.child(senderUserID).child(receiverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    friendRequestRef.child(receiverUserID).child(senderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                sendFriendRequestBtn.setEnabled(true);
                                currentState = "not_friends";
                                sendFriendRequestBtn.setText("Send friend request");

                                declineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                declineFriendRequestBtn.setEnabled(false);
                            }
                        }
                    });
                }
            }
        });


    }

    private void maintananceOfButton() {

        friendRequestRef.child(senderUserID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(receiverUserID)){
                    String requestType = dataSnapshot.child(receiverUserID).child("requestType").getValue().toString();

                    if(requestType.equals("sent")){
                        currentState = "request_sent";
                        sendFriendRequestBtn.setText("Cencel friend request");
                        declineFriendRequestBtn.setVisibility(View.INVISIBLE);
                        declineFriendRequestBtn.setEnabled(false);
                    } else if (requestType.equals("received")){
                        currentState = "request_received";
                        sendFriendRequestBtn.setText("Accepted friend request");
                        declineFriendRequestBtn.setEnabled(false);
                        declineFriendRequestBtn.setVisibility(View.VISIBLE);

                        declineFriendRequestBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cancelFriendRequest();
                            }
                        });
                    }
                } else {
                    friendsRef.child(senderUserID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChild(receiverUserID)){
                                currentState = "friends";
                                sendFriendRequestBtn.setText("Unfriend this person");

                                declineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                declineFriendRequestBtn.setEnabled(false);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendFriendRequestToaPerson() {

        friendRequestRef.child(senderUserID).child(receiverUserID).child("requestType").setValue("sent").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    friendRequestRef.child(receiverUserID).child(senderUserID).child("requestType").setValue("received").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                sendFriendRequestBtn.setEnabled(true);
                                currentState = "request_sent";
                                sendFriendRequestBtn.setText("Cancel friend request");

                                declineFriendRequestBtn.setVisibility(View.INVISIBLE);
                                declineFriendRequestBtn.setEnabled(false);


                            }
                        }
                    });
                }
            }
        });

    }

    private void initializeFields() {

        username = findViewById(R.id.person_profile_username);
        userFullName = findViewById(R.id.person_profile_fullname);
        userStatus = findViewById(R.id.person_profile_status);
        userCountry = findViewById(R.id.person_profile_country);
        userDoB = findViewById(R.id.person_profile_dateOfBirth);
        userRelationShip = findViewById(R.id.person_profile_relationship_status);

        userProfImage = findViewById(R.id.person_profile_picture);
        userGender = findViewById(R.id.person_profile_gender);

        sendFriendRequestBtn = findViewById(R.id.person_profile_friend_request_btn);
        declineFriendRequestBtn = findViewById(R.id.person_profile_decline_friend_req_btn);

        currentState ="not_friends";



    }


}
