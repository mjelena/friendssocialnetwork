package com.example.jelena.friendssocialnetwork;

import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentsActivity extends AppCompatActivity {

    private ImageButton postCommentButton;

    private EditText commentInputText;
    private RecyclerView commentsList;
    private CircleImageView commentUserImage;

    private String postKey;
    private DatabaseReference usersRef, postRef;
    private String currentUserID;
    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        postKey = getIntent().getExtras().get("postKey").toString();
        usersRef  = FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        postRef = FirebaseDatabase.getInstance().getReference().child("Posts").child(postKey).child("Comments");

        postCommentButton = findViewById(R.id.post_comment_button);
        commentInputText = findViewById(R.id.comment_input);
        commentUserImage = findViewById(R.id.comment_profile_image);


        commentsList = findViewById(R.id.comments_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        commentsList.setLayoutManager(linearLayoutManager);


        postCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usersRef.child(currentUserID).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            String username = dataSnapshot.child("username").getValue().toString();
                            String profileImage = dataSnapshot.child("profileImage").getValue().toString();

                            validateComment(username, profileImage);
                            commentInputText.setText("");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });


        displayAllComments();
    }

    private void displayAllComments() {



        FirebaseRecyclerOptions<Comments> options =
                new FirebaseRecyclerOptions.Builder<Comments>()
                        .setQuery(postRef, Comments.class)
                        .build();

        FirebaseRecyclerAdapter<Comments,CommentsViewHolder > firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Comments, CommentsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull CommentsViewHolder holder, int position, @NonNull Comments model) {
                final String commentKey;
                commentKey = getRef(position).getKey();

                holder.setComment(model.getComment());
                holder.setUsername(model.getUsername());
                holder.setDate(model.getDate());
                holder.setTime(model.getTime());
                holder.setProfileImage(model.getProfileImage());


                if (currentUserID.equals(model.getUid())) {
                    if(holder.getDeleteComment()!=null) {
                        holder.getDeleteComment().setVisibility(View.VISIBLE);

                    }
                } else {
                    if(holder.getDeleteComment()!=null) {
                        holder.getDeleteComment().setVisibility(View.INVISIBLE);
                    }
                }

                holder.getDeleteComment().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                         deleteMyComment(commentKey);
                    }
                });

            }

            @NonNull
            @Override
            public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_comment_layout, parent, false);
                CommentsActivity.CommentsViewHolder commentsViewHolder = new CommentsActivity.CommentsViewHolder(view);
                return commentsViewHolder;
            }
        };
        commentsList.setAdapter(firebaseRecyclerAdapter);
        firebaseRecyclerAdapter.startListening();
    }

    private void deleteMyComment(final String commentKey) {

        Log.i("TAG",postRef.child(currentUserID).child("Comments").toString() );
        Log.i("TAG",postRef.toString() );
        Log.i("TAG", commentKey);

                postRef.child(commentKey).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            sendToMainActivity();
                        }
                    }
                });

            }


    private void sendToMainActivity() {
        Intent mainIntent = new Intent(CommentsActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }



    public static class CommentsViewHolder extends RecyclerView.ViewHolder {

        View mView;
        ImageButton deleteComment;

        public ImageButton getDeleteComment() {
            return deleteComment;
        }

        public void setDeleteComment(ImageButton deleteComment) {
            this.deleteComment = deleteComment;
        }

        public CommentsViewHolder(View itemView) {
            super(itemView);
            this.mView = itemView;
            deleteComment = mView.findViewById(R.id.delete_post_comment);
        }

        public void setUsername(String username) {
            TextView usernameComment =  mView.findViewById(R.id.comment_username);
            usernameComment.setText(username+ " ");
        }


        public void setTime(String time) {
            TextView commentTime = mView.findViewById(R.id.comment_time);
            commentTime.setText("   " + time);
        }

        public void setDate(String date) {
            TextView commentDate = mView.findViewById(R.id.comment_date);
            commentDate.setText("   " + date);
        }

        public void setComment(String comment) {
            TextView commentText = mView.findViewById(R.id.comment_text);
            commentText.setText(comment);
        }

        public void setProfileImage(String profileImage) {
            CircleImageView commentImage = mView.findViewById(R.id.comment_profile_image);
            Picasso.get().load(profileImage).into(commentImage);
        }



    }

    private void validateComment(String username, String profileImage) {
        String commentText = commentInputText.getText().toString();
        if(TextUtils.isEmpty(commentText)){
            Toast.makeText(this, "Please write text to comment.", Toast.LENGTH_SHORT).show();
        } else {

            Calendar calForDate = Calendar.getInstance();
            SimpleDateFormat currentData = new SimpleDateFormat("dd-MMMM-yyyy");
            final String saveCurrentData = currentData.format(calForDate.getTime());


            Calendar calForTime = Calendar.getInstance();
            SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
            final String saveCurrentTime = currentTime.format(calForDate.getTime());

            final String randomKey = currentUserID + saveCurrentData + saveCurrentTime;

            HashMap commentsMap = new HashMap();
            commentsMap.put("uid", currentUserID);
            commentsMap.put("comment", commentText);
            commentsMap.put("date", saveCurrentData);
            commentsMap.put("time", saveCurrentTime);
            commentsMap.put("username", username);
            commentsMap.put("profileImage", profileImage);

            postRef.child(randomKey).updateChildren(commentsMap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                        Toast.makeText(CommentsActivity.this, "You have commented successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CommentsActivity.this, "Error occured. Try again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
