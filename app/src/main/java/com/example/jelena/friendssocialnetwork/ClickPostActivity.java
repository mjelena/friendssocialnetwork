package com.example.jelena.friendssocialnetwork;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ClickPostActivity extends AppCompatActivity {

    private ImageView postImage;
    private TextView postDescription;
    private Button buttonDeletePost;
    private Button buttonEditPost;
    private DatabaseReference clickPostRef;
    private FirebaseAuth mAuth;
    private String currentUserID;
    private String databaseUserID;
    private String pDescription, pImage;

    private String postKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_post);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();

        postKey = getIntent().getExtras().get("postKey").toString();
        clickPostRef = FirebaseDatabase.getInstance().getReference().child("Posts").child(postKey);

        postImage = findViewById(R.id.click_post_image);
        postDescription = findViewById(R.id.click_post_description);
        buttonDeletePost = findViewById(R.id.click_button_post_delete);
        buttonEditPost = findViewById(R.id.click_button_edit_post);

        buttonDeletePost.setVisibility(View.INVISIBLE);
        buttonEditPost.setVisibility(View.INVISIBLE);

        clickPostRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

               if(dataSnapshot.exists()) {
                   pDescription = dataSnapshot.child("description").getValue().toString();
                   pImage = dataSnapshot.child("postImage").getValue().toString();
                   databaseUserID = dataSnapshot.child("uid").getValue().toString();

                   postDescription.setText(pDescription);
                   Picasso.get().load(pImage).into(postImage);

                   if (currentUserID.equals(databaseUserID)) {
                       buttonDeletePost.setVisibility(View.VISIBLE);
                       buttonEditPost.setVisibility(View.VISIBLE);
                   }


               }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        buttonEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSelectedPost(pDescription);
            }
        });

        buttonDeletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSelectedPoste();
            }
        });

    }

    private void editSelectedPost(String pDescription) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ClickPostActivity.this);
        builder.setTitle("Edit post");

        final EditText inputField = new EditText(ClickPostActivity.this);
        inputField.setText(pDescription);
        builder.setView(inputField);

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clickPostRef.child("description").setValue(inputField.getText().toString());

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        Dialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.background_light);
    }

    private void deleteSelectedPoste() {

       clickPostRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.i("TAG", "Izbrisana");
                    sendUserToMainActivity();

                    Toast.makeText(ClickPostActivity.this, "Post has been deleted.", Toast.LENGTH_SHORT ).show();
                }
            }
        });


    }

    private void sendUserToMainActivity() {
        Intent mainIntent = new Intent(ClickPostActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();

    }
}

