Android aplikacija je radjena u Android studiju i koristi google platformu Firebase.
Aplikacija je drustvena mreza gdje svaki korisnik ima svoj profil koji moze da napravi pomocu email adrese I passworda ili 
da se uloguje direktno pomocu google accounta.
Prilikom logovanja potrebno je da postavi svoju profilnu sliku I username. 
Unutar aplikacije ima mogucnost da izmeni osnovne informacije o sebi. 
Svaki korisnik moze da postavi sliku, da komentarise I lajkuje. 
Takodje, moze da pretrazuje ljude koji koriste aplikaciju I posalje im zahtev za prijateljstvo. 
Ukoliko dobije zahtev za prijateljstvo korisnik ce biti obavesten notifikacijom. Prijatelji medjusobno mogu da razmenjuju poruke.